﻿using System;
using System.Management;

namespace GetSharePermission
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: GetSharePermission.exe (ComputerName or IPAddress)");
                Environment.Exit(1);
            }
            ShareSecurity(args[0]);
        }
 
        private static void ShareSecurity(string ServerName)
        {
            ConnectionOptions myConnectionOptions = new ConnectionOptions();

            myConnectionOptions.Impersonation = ImpersonationLevel.Impersonate;
            myConnectionOptions.Authentication = AuthenticationLevel.Packet;

            ManagementScope myManagementScope =
                new ManagementScope(@"\\" + ServerName + @"\root\cimv2", myConnectionOptions);

            myManagementScope.Connect();

            if (!myManagementScope.IsConnected)
                Console.WriteLine("could not connect");
            else
            {
                ManagementObjectSearcher myObjectSearcher =
                    new ManagementObjectSearcher(myManagementScope.Path.ToString(), "SELECT * FROM Win32_LogicalShareSecuritySetting");

                Console.WriteLine("\"Name\",\"User\",\"Type\",\"Value\"");

                foreach (ManagementObject share in myObjectSearcher.Get())
                {
                    string dir = share["Name"].ToString();
                    InvokeMethodOptions options = new InvokeMethodOptions();
                    ManagementBaseObject outParamsMthd = share.InvokeMethod("GetSecurityDescriptor", null, options);
                    ManagementBaseObject descriptor = outParamsMthd["Descriptor"] as ManagementBaseObject;
                    ManagementBaseObject[] dacl = descriptor["DACL"] as ManagementBaseObject[];

                    foreach (ManagementBaseObject ace in dacl)
                    {
                        try
                        {
                            ManagementBaseObject trustee = ace["Trustee"] as ManagementBaseObject;
                            string user = trustee["Domain"] as string + @"\" + trustee["Name"] as string;
                            string accessMask = AccessMaskToString(ace["AccessMask"].ToString());
                            string aceType = AceTypeToString(ace["AceType"].ToString());
                            Console.WriteLine("\"" + dir + "\",\"" + user + "\",\"" + accessMask + "\",\"" + aceType + "\"");
                        }
                        catch (Exception error)
                        {
                            Console.WriteLine("Error: " + error.ToString());
                        }
                    }
                }
            }
        }

        private static string AccessMaskToString(string accessMask)
        {
            switch(accessMask)
            {
                case "2032127":
                    return "FullControl";
                case "1179785":
                    return "Read";
                case "1179817":
                    return "Read, Write";
                case "-1610612736":
                    return "ReadAndExecute";
                case "1245631":
                    return "ReadAndExecute, Modify, Write";
                case "1180095":
                    return "ReadAndExecute, Write";
                case "268435456":
                    return "FullControl (Sub Only)";
                default:
                    return accessMask;
            }
        }

        private static string AceTypeToString(string aceType)
        {
            switch(aceType)
            {
                case "0":
                    return "Allow";
                case "1":
                    return "Deny";
                case "2":
                    return "Audit";
                default:
                    return aceType;
            }
        }
    }
}
