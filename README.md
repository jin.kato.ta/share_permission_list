# SharePermissionList

Windowsのファイル共有の権限リストを出力するプログラム

## コマンド

```bash
SharePermissionList.exe (コンピュータ名)
```

### 引数
|No|引数名|説明|
|--|--|--|
|1|コンピュータ名|コンピュータ名、または、IPアドレスを指定します。|

### 出力

カンマ区切りのCSV形式で結果が標準出力されます。

|列|説明|
|--|--|
|Name|指定のコンピュータで共有される共有名称|
|User|権限ユーザ、または、グループ|
|Type|権限種別|
|Value|権限種別における値|

## 使い方

1. 最新の[リリース](https://gitlab.com/jin.kato.ta/share_permission_list/-/releases)から`SharePermissionList.exe`をダウンロードし、任意のディレクトリに配置します。
1. コマンドプロンプトを起動します。
1. `SharePermissionList.exe`を配置したディレクトリにカレントディレクトリを移動します。  
   例：`SharePermissionList.exe`を`C:\Users\User\Desktop`に配置した場合
   ```bash
   cd C:\Users\User\Desktop
   ```
1. `SharePermissionList.exe`を実行します。
   ```bash
   SharePermissionList.exe (コンピュータ名) > (出力ファイル名.csv)
   ```
   例：
   ```
   SharePermissionList.exe sharedcomputer > result.csv
   ```
1. 上記にて指定した出力ファイル名に結果が出力されます。

## 注意事項

- PowerShell上で実行すると、出力されるCSVの文字コードがUnicordのため、Excel上で表示するときにうまく表形式になりません。コマンドプロンプト上で実行してください。
